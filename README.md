# Infra

Terraform provider
------------

- Documentation: https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs

Requirements
------------

- [Terraform](https://www.terraform.io/downloads.html) 0.12+
- [Ansible core](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) 2.11+

## Сloning a repository

```sh
$ git clone git@gitlab.com:skillbox38/infra.git
```

## Deploying infrastructure with Terraform

### Parameters must be set as environment variables
```
export TF_VAR_token="<yandex_token>"
export TF_VAR_cloud_id="<yandex_cloud_id>"
export TF_VAR_folder_id="<yandex_folder_id>"
export TF_VAR_domain="<domain_name>"
export gitlab_runner_host="<gitlab_runner_host>"
export gitlab_runner_token="<gitlab_runner_token>"
export grafana_user="<grafana_user>"
export grafana_password="<grafana_password>"
```
### Сommands to initialize the infrastructure

```sh
$ cd infra/
$ terraform init
$ terraform plan
$ terraform apply
```


### Importing the Prometheus Stats Dashboard

This section will download an official, pre-built Prometheus Stats Dashboard and instruct you on how to import it into Grafana.

Download the latest Prometheus Dashboard one of two ways:
------------

1. Right click and save the following link: Prometheus Stats - Default Grafana Dashboard
2. Navigate your browser directly to the dashboard JSON file: http://grafana.org/assets/dashboards/prometheus-dash.json

The import view can be found at the Dashboard Picker dropdown, next to the New Dashboard and Playlist buttons.

To import a dashboard from a local JSON file, click the Choose file button in the Import File section. Find the downloaded prometheus-dash.json on your local file system, and import it.

Note: If you have named your data source something other than Prometheus, you will need to do a find and replace the data source name in the .json file. Open it with:

```sh
$ nano prometheus-dash.json
```

Find the following, and change the data source filename from Prometheus to whatever you named it:

```sh
"datasource": "Prometheus",
```

After importing the Prometheus Dashboard, you will immediately be taken to the Prometheus Stats Dashboard, and if everything is properly configured, you will start to see statistics from your Prometheus server.

Important: Click the Save button in the top menu to save your dashboard within your Grafana instance. If you do not save your dashboard, it will not appear in your Grafana after you close your browser.