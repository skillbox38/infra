# 1. Choice of monitoring system

Date: 2022-10-21

## Status

Accepted

## Context

We need to choose a monitoring system.

## Decision

[Prometheus](https://prometheus.io/) is a popular National Source Code monitoring tool that many companies use to monitor their IT infrastructure. The main advantages of [Prometheus](https://prometheus.io/) are tight integration with Kubernetes and many available exporters and client libraries, as well as a fast query language and API.

## Consequences

Additional control over the procedure for choosing a data discovery system.

